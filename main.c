#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <sys/time.h>
#include <memory.h>

static const long Num_To_Sort = 10000;
#define omp_get_max_threads 10

// Sequential version of your sort
// If you're implementing the PSRS algorithm, you may ignore this section
void merge_ser(int* arr, int left, int mid, int right, int *temp)
{
    int i = left;
    int j = mid + 1;
    int k = left;
    while (i <= mid && j <= right)
    {
        if (arr[i] < arr[j])
        {
            temp[k++] = arr[i++];
        }
        else
        {
            temp[k++] = arr[j++];
        }
    }
    while (i <= mid)
    {
        temp[k++] = arr[i++];
    }
    while (j <= right)
    {
        temp[k++] = arr[j++];
    }

    memcpy(arr + left, temp + left, sizeof(int) * (right - left + 1));
}

void sort_s(int* arr, int left, int right, int *temp)
{

    if (left < right)
    {
        int mid = left + (right - left) / 2;
        sort_s(arr, left, mid, temp);
        sort_s(arr, mid + 1, right, temp);
        merge_ser(arr, left, mid, right, temp);
    }
}

// Parallel version of your sort

void merge_par(int* arr, int left, int mid, int right, int *temp)
{
    int i = left;
    int j = mid + 1;
    int k = left;
#pragma omp parallel
    while (i <= mid && j <= right)
    {
        if (arr[i] < arr[j])
        {
            temp[k++] = arr[i++];
        }
        else
        {
            temp[k++] = arr[j++];
        }
    }
    while (i <= mid)
    {
        temp[k++] = arr[i++];
    }
    while (j <= right)
    {
        temp[k++] = arr[j++];
    }

    memcpy(arr + left, temp + left, sizeof(int) * (right - left + 1));
}

void sort_p(int* arr, int left, int right, int *temp)
{

    if (left < right)
    {
        int mid = left + (right - left) / 2;
#pragma omp parallel sections
        {
#pragma omp section
            sort_p(arr, left, mid, temp);
#pragma omp section
            sort_p(arr, mid + 1, right, temp);
#pragma omp section
            merge_par(arr, left, mid, right, temp);
        }
    }
}



int main() {
    int *arr_s = malloc(sizeof(int) * Num_To_Sort);
    int *temp = malloc(sizeof(int) * Num_To_Sort);
    long chunk_size = Num_To_Sort / omp_get_max_threads;
#pragma omp parallel num_threads(omp_get_max_threads)
    {
        int p = 1;
        p++;
        unsigned int seed = (unsigned int) time(NULL) + (unsigned int) p;
        long chunk_start = p * chunk_size;
        long chunk_end = chunk_start + chunk_size;
        for (long i = chunk_start; i < chunk_end; i++) {
            arr_s[i] = rand_r(&seed);
        }
    }

    // Copy the array so that the sorting function can operate on it directly.
    // Note that this doubles the memory usage.
    // You may wish to test with slightly smaller arrays if you're running out of memory.
    int *arr_p = malloc(sizeof(int) * Num_To_Sort);
    memcpy(arr_p, arr_s, sizeof(int) * Num_To_Sort);

    struct timeval start, end;

    printf("Timing sequential...\n");
    gettimeofday(&start, NULL);
    sort_s(arr_s,0,Num_To_Sort-1,temp);
    gettimeofday(&end, NULL);
    printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000);

    free(arr_s);

    printf("Timing parallel...\n");
    gettimeofday(&start, NULL);
    sort_p(arr_p,0,Num_To_Sort-1,temp);
    gettimeofday(&end, NULL);
    printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000);
    free(temp);
    free(arr_p);

    return 0;
}
